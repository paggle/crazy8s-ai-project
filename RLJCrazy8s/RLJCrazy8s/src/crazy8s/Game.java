package crazy8s;

import crazy8s.card.Card;
import static crazy8s.card.Card.CLUBS;
import static crazy8s.card.Card.DIAMONDS;
import static crazy8s.card.Card.HEARTS;
import static crazy8s.card.Card.SPADES;
import crazy8s.card.Deck;
import crazy8s.player.AdvancedOpponent;
import crazy8s.player.Command;
import crazy8s.player.Human;
import crazy8s.player.IPlayer;
import crazy8s.player.Opponent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Game class which implements Crazy 8s play rules.
 *
 * @author Liam Harwood and Juan Vasquez
 */
public class Game {   
    protected ArrayList<IPlayer> players;

    protected Deck deck;
    protected static HashMap<String,Integer> wins = new HashMap<>();
    protected static Boolean interactive = true;
    protected static Integer rounds = Integer.MAX_VALUE;

    /**
     * Constructor
     */
    public Game() {
        players = new ArrayList<>();
        players.add(interactive ? new Human() : new Opponent());
        players.add(new AdvancedOpponent());
        //players.add(new Opponent());
        Collections.shuffle(players);
        deck = new Deck();
    }

    /**
     * Decodes integer value representing suit and converts it to a string
     *
     * @param suit Integer representing suit
     * @return Decoded string
     */
    protected String decodeSuit(int suit) {
        if (suit == CLUBS) {
            return "Clubs";
        }

        if (suit == DIAMONDS) {
            return "Diamonds";
        }

        if (suit == HEARTS) {
            return "Hearts";
        }

        if (suit == SPADES) {
            return "Spades";
        }

        return "";
    }

    /**
     * Deals cards to both players
     */
    protected void deal() {
        for (int index = 0; index < 8; index++) {
            for (IPlayer player : players) {
                ArrayList<Card> hand = player.getHand();
                Card card = deck.draw();
                hand.add(card);
            }
        }
        
        Card card = deck.draw();
        
        deck.discard(card);
        
        for (IPlayer player : players) {
            player.played(player, card);
        }

    }

    /**
     * Runs the command to play a card
     *
     * @param player Player playing a card
     * @param index Index of card in hand to be played
     */
    protected void doPlay(IPlayer player, int index) {
        Card card = player.getHand().remove(index);
        
        logger(player + " played " + card);
        
        if (card.getRank() == 8) {
            int suit = player.getSuit();
            
            for (IPlayer eachPlayer : players) {
                eachPlayer.setSuit(suit);
            }
            
            player.setSuit(suit);
            
            card.setSuit(suit);
            
            logger(player + " changed discard suit to " + card.decodeSuit());
        }
        deck.discard(card);
        
        for (IPlayer eachPlayer : players) {
            eachPlayer.played(player, card);
        }
        
        if (player instanceof Human) {
            doShow(player);
        }
    }

    /**
     * Runs the command to draw a card
     *
     * @param player Player drawing a card
     */
    protected void doDraw(IPlayer player) {
        Card card = deck.draw();
        ArrayList<Card> hand = player.getHand();
        hand.add(card);
        for (IPlayer eachPlayer : players) {
            eachPlayer.drew(player, card);
        }
        
     
        logger(player + " drew a card.");
        
        if (player instanceof Human) {
            doShow(player);
        }

    }

    /**
     * Runs the command to show discard card, score and cards in hand
     *
     * @param player Player giving command
     */
    protected void doShow(IPlayer player) {
        if(!interactive)
            return;
        
        Card discard = new Card(deck.getDiscardRank(), deck.getDiscardSuit());
        logger("Discard card is " + discard);

        System.out.println(player);
        logger("Score: " + player.getScore());

        ArrayList<Card> hand = player.getHand();
        for (int index = 0; index < hand.size(); index++) {
            Card card = hand.get(index);
            logger((index + 1) + ". " + card);
        }
    }

    /**
     * Runs the command to show discard card, score, cards in hand, number of
     * cards in deck, number of cards in discard pile, and number of cards in
     * other player's hand
     *
     * @param player Player giving command
     */
    protected void doRefresh(IPlayer player) {
        doShow(player);
        logger("Number of cards in deck: " + deck.cards.size());
        logger("Number of cards in discard pile: " + deck.discards.size());
        logger("Number of cards in opponent's hand: " + player.getOPHS());
        

    }

    /**
     * Sums up the card values in losing player's hand at end of game to
     * determine score to be added to winner's score
     *
     * @param player Player who won
     * @return Score to be added
     */
    protected int sumScore(IPlayer player) {
        int score = 0;
        for (IPlayer p : players) {
            for (Card card : p.getHand()) {
                if (card.getRank() == 8) {
                    score += 50;
                } else if (card.getRank() > 9) {
                    score += 10;
                } else {
                    score += card.getRank();
                }

            }
        }

        return score;
    }

    /**
     * Checks if player goes out and wins the game, adds to their score
     *
     * @param player Player who played a card
     * @return 0 if they still have cards, 1 if they won the game
     */
    protected int checkOut(IPlayer player) {
        if (player.getHand().size() < 1) {
            logger("Game over");
            
            int score = sumScore(player);
            logger(player + " won and earned " + score + " points!");
            
            player.setScore(player.getScore() + score);
            
            for (IPlayer p : players) {
                logger(p + " score:" + p.getScore());
                if(p == player) {
                    Integer count = wins.getOrDefault(p.toString(), 0);
                    count++;
                    wins.put(p.toString(), count);
                }
            }
            return 1;
        }
        return 0;
    }

    /**
     * Starts a new game
     */
    protected void newGame() {
        Game game = new Game();
        game.go();
    }

    /**
     * Main game loop
     */
    protected void loop() {
        while (true) {
            playerLoop:
            for (IPlayer player : players) {
                do {
                    int command = player.getCommand();
                    switch (command) {
                        case Command.SHOW:
                            doShow(player);
                            break;
                        case Command.REFRESH:
                            doRefresh(player);
                            break;
                        case Command.DRAW:
                            doDraw(player);
                            break;
                        case Command.QUIT:
                            System.exit(0);
                        case Command.NO_COMMAND:
                            System.err.println("Invalid command");
                            continue;
                        default:
                            doPlay(player, command);
                            if (checkOut(player) == 1) {
                                return;
                            }
                            continue playerLoop;
                    }
                } while (true);
            }
        }
    }

    /**
     * Deals card, shows instructions and starts main game loop
     */
    public void go() {
        deal();
        logger("New game:");
        logger("Commands:");
        logger("s = Shows discard card, your score, and cards in your hand");
        logger("r = Does 's' command and shows number of cards in the deck, the discard pile, and the opponent's hand");
        logger("d = Draws a card");
        logger("q = Quits the game");
        logger("Index number of card in hand = Plays that card");
    
        loop();
    }

    protected static void logger(String msg) {
        if(interactive)
            System.out.println(msg);
    }
    
    /**
     * Play the game
     *
     * @param args Command line arguments
     */
    public static void main(String[] args) {  
        int n = Integer.MAX_VALUE;
        if(args.length != 0) {
           n = rounds = Integer.parseInt(args[0]);
            interactive = false;
        }
        while (rounds-- != 0) {
            Game game = new Game();
            logger("Welcome to Crazy 8s!");
            game.go();
        }
        System.out.println("N: "+n+" ");
        
        for(String name: wins.keySet()) {
            int observed = wins.get(name);
            String statSig;           
            double z = (observed - n/2.) / Math.sqrt(n*0.5*0.5);
            //double winFreq = (observed / n) * 100;
            statSig = (z > 1.96 || z < -1.96)? "statistically significant" : "not statistically significant";
            System.out.printf("%s wins: %d z: %6.4f %s\n", name,observed,z,statSig);
        }
    }

}
