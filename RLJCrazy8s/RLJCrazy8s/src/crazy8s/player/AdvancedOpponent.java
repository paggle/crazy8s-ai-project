/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crazy8s.player;

import crazy8s.card.Card;

/**
 *
 * @author Stephen Pagliuca
 */
public class AdvancedOpponent extends Opponent {
    public final static String NAME = "ADVANCED OPPONENT";
    //keeps track of which suit made opponent draw
    public int cardMadeDraw;
    /**
     * 
     * @return name of the player  
     */
    @Override
    public String toString() {
        return NAME;
    }
    /**
     * 
     * @return the index of the card to play or the draw command 
     */
    @Override
    public int getCommand() {
        int suit = 0;
        if (discarded.getRank() != 8) {
            if(discarded.getSuit() == cardMadeDraw){
                suit = discarded.getSuit();
                int index = searchSuit(suit);
                if (index != -1) {
                    return index;
                }
            }
            if(discarded.getSuit() == chooseSuit()){
                suit = discarded.getSuit();
                int index = searchSuit(suit);
                if (index != -1) {
                    return index;
                }
            }
            int index = searchRank(discarded.getRank());
            if (index != -1) {
                return index;
            }
        }
        else{
            suit = suit8;
        }
            if (suit == 0) {
                suit = discarded.getSuit();
            }
            
            int index = searchSuit(suit);
            if (index != -1) {
                return index;
            }
            index = search8s();
            if (index == -1) {
                return Command.DRAW;
            }
            return index;
    }
    /**
     * 
     * @param player that drew a card
     * @param card that was drawn 
     * keeps track of the suit that made the player draw
     */
    @Override
    public void drew(IPlayer player, Card card) {
        if (player != this) {
            this.otherPlayerHandSize++;
            cardMadeDraw = discarded.getSuit();
        }
    }
}
